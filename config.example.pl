# Our nick
botnick => 'AMPd',
# ident (aka user name)
ident => 'amp',
# real name (aka ircname)
ircname => 'vHost management',
# NickServ password
nspass => 'CHANGEME',
# Do we want to oper up?
# 0 = no
# 1 = yes
operlogin => 0,
# Oper user name
operuser => 'TheAMPdBot',
# Oper password
operpass => 'soopahs3cur3p4ssw0rd',
# Target server
server => 'irc.example.net',
# Target server port
port => 6667,
# Use SSL to connect to target server?
# 0 = no
# 1 = yes
# Using SSL is highly recommended if the bot has to join the regular
# services log channel instead of a dedicated host request log channel.
usessl => 0,
# vHost Service nick
servnick => 'HostServ',
# Channel(s) to listen for requests in.
# Multiple channels are possible.
# With key: reqchannel = [ '#chan' => 'key' ],
reqchannel => [ '#v' ],
# Message set
# Anope 1.9: anope19,
# Atheme:    atheme,
msgset => 'anope19',
# Date format.
# This is used to give the added regexes a date.
# See also: man date(1)
datefmt => '%m/%d/%y',
# Fork away? If set, the AMPd will actually fork into the background
# instead of hogging a console. You'll probably want this on any sort
# of live network
fork => 1,
# vim: set expandtab sw=4 ts=4
