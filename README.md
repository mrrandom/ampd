AMPd
====

Description
-----------

AMPd, the Anope Misconfiguration Prevention daemon, is a
POE::Component::IRC based bot. Currently, it only handles Anope 1.9 and
Atheme host requests (which are preferably logged outside the regular
log channel if there is one).

Since Anope 1.9.6 removed vHost restrictions and opers may want to, for
example, use colored vHosts but not let any users use them, they'd need
to either reject all of them manually, or they could use a bot doing it
for them; like this one. This is _always_ to be considered beta software,
you get no warranty and you have no right to complain over bad code.

Currently supposed to be implemented is:

* Rejecting vHost requests based on regex
* Rejecting vHost requests based on per-nick bans
* An SQLite database for bans/regex

Dependencies
------------

* POE::Component::IRC
* DBI
* DBD::SQLite
* Getopt::Std
* POSIX

Installation
------------

Just unpack the tarball, it's not that hard. You most likely succeeded
doing that anyway.

Then copy config.example.pl to config.pl and edit config.pl. Please
note that the entire configuration file is interpreted with do(), so
you can squeeze in some Perl if you somehow manage to do so. Just make
sure you check the syntax with `perl -c config.pl` before running.

On the services, give the bot permissions to do hostserv reject, it
won't require more. If you already have a hostsetter/helper operator
class, just shove it there. Make sure you put it in a separate nick
group/account under all circumstances, though.

Usage
-----

./ampd.pl

The bot will accept commands adressed to it (nick: command) and commands
issued via private message. List of currently implemented commands:

* `help`
* `die`
* `rehash`
* `regex {add|del|list} regexp`
* `ban {add|del|list} nick [reason]`

All commands _must_ be issued from a user who is in one of the
configured channels, or they will go ignored. Even `help`. For that reason,
you would do well in not adding a BotServ bot or at least giving nobody
access to the BotServ SAY command unless you want people to anonymously
add bans and regexes without anyone knowing who's doing it.

It might be a smart idea to add some regex to be rejected, too. Otherwise,
AMPd won't be doing much. Which isn't the point.

Regarding bans, the reason only has to be specified when adding bans.

License
-------

See COPYING. If you can't be bothered, it's licensed under the WTFPL.

Troubleshooting
---------------

Check if you actually properly installed POE::Component::IRC (in a 
preferably recent version). Check your configuration twice. If that
doesn't yield any positive results, read your configuration again. In
the unlikely event that your configuration is absolutely right but
everything fails anyway, contact me at cculex.AT.gmail.DOT.com.

