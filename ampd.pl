#!/usr/bin/env perl
# vim: set expandtab sw=4 ts=4
use strict;
use warnings;
use POE;
use POE::Component::IRC::State;
use POE::Component::IRC::Plugin::AutoJoin;
use POE::Component::IRC::Plugin::BotCommand;
use POE::Component::IRC::Plugin::NickServID;
use DBI;
use POSIX qw(strftime);
use Getopt::Std;
use utf8;

my $VERSION = "0.8";
my $cfg_file_name = "config.pl";
my %opts;

getopts('dhvc:', \%opts) or &usage();
&HELP_MESSAGE() if $opts{h};
&VERSION_MESSAGE() if $opts{v};

$cfg_file_name = $opts{c} if(defined($opts{c}));

sub cfg_read {
    my $cfg_file = $_[0];
    my %cfg = do($cfg_file);

    die $! if $!;

    if ($@) {
        chomp $@;
        die "Couldn't read $cfg_file: $@";
    }

    return %cfg;
}

my %config = &cfg_read($cfg_file_name);

# Initialize db if necessary
my $dbh = DBI->connect("dbi:SQLite:dbname=ampd.db","","")
          or die "Couldn't open ampd.db with SQLite.";
# I'm not using ids because I don't feel it's necessary. Besides, it's
# better to make sure that the regex you delete is the one you mean, and
# not some silly typo.
$dbh->do("CREATE TABLE IF NOT EXISTS rejectregex (
    regex TEXT UNIQUE,
    owner TEXT,
    date  TEXT)");
$dbh->do("CREATE TABLE IF NOT EXISTS bans (
    nick  TEXT UNIQUE,
    reason TEXT,
    owner TEXT,
    date  TEXT)");
# Preparations done at program start because we can waste infinite
# amounts of time at this tage.
my $getregex = $dbh->prepare("SELECT regex,owner,date FROM rejectregex");
my $addregex = $dbh->prepare("INSERT INTO rejectregex(regex,owner,date) VALUES (?,?,?)");
my $delregex = $dbh->prepare('DELETE FROM rejectregex WHERE regex = ?');
my $getbans = $dbh->prepare("SELECT nick,reason,owner,date FROM bans");
my $addban = $dbh->prepare("INSERT INTO bans(nick,reason,owner,date) VALUES (?,?,?,?)");
my $delban = $dbh->prepare('DELETE FROM bans WHERE nick = ?');

if($config{fork} && !$opts{d}) {
    my $pid = fork;
    die "Failed to fork: $!" if !defined($pid);
    exit if $pid;
} else {
    # We'll run a silly debug scenario all the time anyway.
    use Data::Dumper;
}

# We actually don't need any of those messages but Request for now, but
# who knows if AMPd might grow?
our %msgset = (
    anope19 => {
        # $1 = nick requesting
        # $2 = vhost <-- WE NEED TO CHECK THIS!
        Request => '^COMMAND: (.+)!.+@.+? used request to request new vhost (.+)$',
        # $1 = nick whose request was activated
        Accept  => '^COMMAND: .+!.+@.+? used activate for (.+) for vhost .+$',
        # $1 = nick whose request was rejected
        Reject  => '^COMMAND: .+!.+@.+? used reject to reject vhost for (.+) \(.*\)$',
    },
    atheme  => {
        # Who had the retarded idea of using bolds here?!
        Request => '^(.+)(?: \(.+\))? REQUEST: (.+)$',
        Accept  => '^.+(?: \(.+\))? ACTIVATE: .+ for (.+)$',
        Reject  => '^.+(?: \(.+\))? REJECT: .+ for (.+)$',
    },
);

my $bot = POE::Component::IRC::State->spawn(
    Nick      => $config{botnick},
    Username  => $config{ident},
    Ircname   => $config{ircname},
    Server    => $config{server},
    Port      => $config{port},
    UseSSL    => $config{usessl},
    NoDNS     => 1,
    Flood     => 1,
) or die "Error: $!";

POE::Session->create(
    package_states => [
        main => [ qw(_start irc_identified irc_public irc_ctcp_version irc_error irc_shutdown irc_botcmd_rehash irc_botcmd_regex irc_botcmd_ban irc_botcmd_die) ]
    ],
    heap => { irc => $bot },
);

$poe_kernel->run();

sub _start {
    my $heap = $_[HEAP];
    my $bot = $heap->{irc};
    $bot->yield(debug => 1) if $opts{d};

    $bot->plugin_add('AutoJoin', POE::Component::IRC::Plugin::AutoJoin->new(
            Channels       => $config{reqchannel},
            RejoinOnKick   => 1,
            Rejoin_delay   => 0,
            NickServ_delay => 2,
        ));
    $bot->plugin_add('BotCommand', POE::Component::IRC::Plugin::BotCommand->new(
            Commands   => {
                rehash => 'Rehash config.pl.',
                die    => 'Exit gracefully.',
                regex  => '{add|del|list} regexp.',
                ban    => '{add|del|list} nick [reason].',
            },
            Auth_sub   => \&auth,
            Bare_private => 1,
            Ignore_unknown => 1,
        ));
    $bot->plugin_add('NickServID', POE::Component::IRC::Plugin::NickServID->new(
            Password => $config{nspass},
        ));

    $bot->yield(register => qw/public identified ctcp_version error shutdown botcmd_rehash botcmd_die botcmd_regex botcmd_ban/);
    $bot->yield('connect');

    return;
}

sub irc_identified {
    if($config{operlogin}) {
        $bot->yield(oper => $config{operuser} => $config{operpass});
    }

    return;
}

sub irc_public {
    my ($sender, $who, $where, $msg) = @_[SENDER, ARG0 .. ARG2];
    my $nick = (split '!', $who)[0];
    my $channel = $where->[0];

    if($nick eq $config{servnick}) {
        # Regexes are slow, but this is Perl. Use something else if you
        # need more speed for a vHost rejecting bot.
        if($msg =~ $msgset{$config{msgset}}{Request}) {
            my $reqnick = $1;
            my $vhost = $2;

            $getbans->execute();
            while(my ($bannednick,$reason) = $getbans->fetchrow_array()) {
                if(lc($reqnick) eq lc($bannednick)) {
                    $bot->yield(notice => $channel => "Rejected host $vhost for banned nick $reqnick.");
                    # Atheme won't show the ban reason; they'll just have to argue with the opers then.
                    $bot->yield(privmsg => "HostServ" => "REJECT $reqnick [Automated] You are banned from the vHost requesting system. Reason: $reason");
                }
            }

            $getregex->execute();
            while(my ($regex) = $getregex->fetchrow_array()) {
                if($vhost =~ /$regex/i) {
                    $bot->yield(notice => $channel => "Rejected host $vhost for $reqnick with match: $regex.");
                    # We can profit from Atheme ignoring any additional arguments with reject!
                    $bot->yield(privmsg => "HostServ" => "REJECT $reqnick [Automated] Matching against forbidden regular expression: $regex");
                }
            }
        }
    }

    return;
}

sub irc_ctcp_version {
    my ($who, $target, $text) = @_[ARG0 .. ARG2];
    my $nick = (split '!', $who)[0];
    $bot->yield(ctcpreply => $nick => "VERSION Anope Misconfiguration Prevention daemon version ${VERSION}");
    return;
}

sub irc_botcmd_die {
    my ($who, $where, $args) = @_[ARG0 .. ARG2];
    my $nick = (split '!', $who)[0];
    $bot->yield('shutdown' => "Shutdown requested by $nick");
    return;
}

sub irc_botcmd_rehash {
    my ($who, $where, $args) = @_[ARG0 .. ARG2];
    # Yes, this means stealthy rehashing. Again, nearly no harm to be
    # done in any way there.
    $bot->yield(notice => $where => "Rehashing configuration file...");
    %config = &cfg_read($cfg_file_name);
    return;
}

sub irc_botcmd_regex {
    my ($who, $where, $args) = @_[ARG0 .. ARG2];
    my $nick = (split '!', $who)[0];
    my ($subcmd,$regex) = (split ' ', $args);
    $subcmd = lc($subcmd);
    if($subcmd eq "list") {
        $getregex->execute();

        while(my ($regex,$owner,$date) = $getregex->fetchrow_array()) {
            $bot->yield(notice => $nick => "$owner [$date]: $regex");
        }
        return;
    }
    elsif($subcmd eq "add") {
        my $rv = $addregex->execute($regex, $nick, strftime($config{datefmt}, localtime));
        if(defined($rv)) {
            $bot->yield(notice => $nick => "Added regex: $regex");
        } else {
            $bot->yield(notice => $nick => "Failed to add regex. Duplicate entry?");
        }
        return;
    }
    elsif($subcmd eq "del") {
        my $rv = $delregex->execute($regex);
        if($rv == 1) {
            $bot->yield(notice => $nick => "Deleted regex: $regex");
        } else {
            $bot->yield(notice => $nick => "Failed to delete regex. Does it exist at all?");
        }
        return;
    }
    else {
        $bot->yield(notice => $nick => "Invalid paramters. Syntax: regex {add|del|list} regexp");
        return;
    }
    return;
}

sub irc_botcmd_ban {
    my ($who, $where, $args) = @_[ARG0 .. ARG2];
    my $nick = (split '!', $who)[0];
    my ($subcmd,$target,$reason) = (split ' ', $args, 3);
    $subcmd = lc($subcmd);
    if($subcmd eq "list") {
        $getbans->execute();

        while(my ($bannednick,$reason,$owner,$date) = $getbans->fetchrow_array()) {
            $bot->yield(notice => $nick => "$owner [$date]: $bannednick ($reason)");
        }
        return;
    }
    elsif($subcmd eq "add") {
        my $rv = $addban->execute($target, $reason, $nick, strftime($config{datefmt}, localtime));
        if(defined($rv)) {
            $bot->yield(notice => $nick => "Added ban: $target ($reason)");
        } else {
            $bot->yield(notice => $nick => "Failed to add ban. Duplicate entry?");
        }
        return;
    }
    elsif($subcmd eq "del") {
        my $rv = $delban->execute($target);
        if($rv == 1) {
            $bot->yield(notice => $nick => "Deleted ban: $target");
        } else {
            $bot->yield(notice => $nick => "Failed to delete ban. Does it exist at all?");
        }
        return;
    }
    else {
        $bot->yield(notice => $nick => "Invalid paramters. Syntax: ban {add|del|list} nick [reason]");
        return;
    }
    return;
}

sub irc_error {
    # If someone really wants to hate on us, they should just use the die
    # command. We'll assume a ping timeout/friendly kill/sad attempt at
    # restarting and, thus, reconnect with the force of a thousand suns.
    if($_[0] =~ /Shutdown requested by/) {
        $bot->yield('shutdown');
    } else {
        $bot->yield('connect');
    }
    return;
}

sub irc_shutdown {
    $dbh->disconnect or warn $dbh->errstr;
    exit;
}

sub auth {
    my $who = $_[1];
    # Auth_sub is run with empty arguments at one time, so make sure to
    # check for that annoying case
    # Also ignore silly requests from services
    unless($who eq "") {
        my $nick = (split '!', $who)[0];
        return 0 if($nick =~ /Serv/);

        for my $chan (@{$config{reqchannel}}) {
            if(POE::Component::IRC::State::is_channel_member($bot, $chan, $nick)) {
                return 1;
            }
        }
    }
    return 0;
}

sub HELP_MESSAGE {
    print STDERR << "EOF";
  -c config_file    use config_file to read the configuration form
  -d                run in debug mode and don't fork away
  -h                print this help message and exit
  -v                print the version message and exit
EOF
    exit;
}

sub VERSION_MESSAGE {
    print << "EOF";
Anope Misconfiguration Prevention daemon $VERSION.

Released under the WTFPL.
EOF
    exit;
}

